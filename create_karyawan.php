<?php include 'templates/header.php'; include 'koneksi.php'; ?>

<div class="container">
  <div class="col-md-6">
    <div style="border: solid 1px ;padding:10px;margin-bottom:5px;background-color:white"><label><b>Input Data Karyawan</b></label></div>
    <div class="box box-primary">
      <div class="box-body">
        <form action="karyawan/input.php" name="modal_popup" enctype="multipart/form-data" method="POST">
          <div class="form-group">
            <input type="text" name="nik"  class="form-control" placeholder="NIK" required/>
          </div>
          <div class="form-group">
            <input type="text" name="nama"  class="form-control" placeholder="NAMA KARYAWAN" required/>
          </div>
          <div class="form-group">
            <label>Jenis Kelamin</label>
            <div class="input-group">
              <select class="form-control" name="jk">
                <option value="Pria">Laki-Laki</option>
                <option value="Wanita">Perempuan</option>
              </select>
            </div><!-- /.input group -->
          </div><!-- /.form group -->
          <div class="form-group">
            <input type="text" name="jabatan"  class="form-control" placeholder="JABATAN" required/>
          </div>
          <div class="form-group">
            <input type="text" name="hp"  class="form-control" placeholder="NO TELEPON" style="width:180px" required/>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="submit">Save</button>
            <button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">Cancel</button>
          </div>
        </form>
      </div>
    </div>
     <?php 
      if ($_GET[status]==1) {
        echo "<b>Sukses, Data Karyawan Berhasil Tersimpan</b>";
      }
    ?>
  </div>
</div>


<?php include 'templates/footer.php';?>