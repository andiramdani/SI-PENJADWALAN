<?php include 'templates/header.php'; include 'koneksi.php'; ?>

<style type="text/css">
    th,td {
      text-align: center;
    }
    table {
      background-color: white;
    }
</style>

<div class="container">
  <div class="row">
    <div class="col-md-11">
      <div class="box box-primary">
        <div class="box-body">
          <label><b>Data Karyawan</b></label>
        </div>
      </div>
      <div class="table-responsive">               
        <table id="example1" class="table table-hover table-bordered" cellspacing="0" width="100%">
          <thead>
            <tr style='background-color: #3c8dbc;color: white;'>
              <th>NO</th>
              <th>NIK</th>
              <th>NAMA KARYAWAN</th>
              <th>JENIS KELAMIN</th>
              <th>JABATAN</th>
              <th>HP</th>
              <th>AKSI</th>
            </tr>
          </thead>
          <tbody>
            <?php
                // panggil koneksi db
                include 'koneksi.php';

                // eksekusi perintah sql cari data karyawan
                $sql  = mysql_query("SELECT * FROM Karyawan");
                $no = 1;
                while ($dp = mysql_fetch_array($sql)) {

                  //tampilkan data karyawan
                  echo "<tr>
                          <td style='text-align:center'>$no</td>
                          <td style='text-align:left'>$dp[nik]</td>
                          <td style='text-align:left'>$dp[nama]</td>
                          <td style='text-align:center'>$dp[jenis_kelamin]</td>
                          <td style='text-align:center'>$dp[jabatan]</td>
                          <td style='text-align:center'>$dp[hp]</td>
                          <td>
                              <a href='karyawan/delete.php?id=$dp[id]' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='DELETE' style='margin-right:10px'><span class='glyphicon glyphicon-trash'></span></a>
                              <a href='edit_karyawan.php?id=$dp[id]' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='EDIT'><span class='glyphicon glyphicon-edit'></span></a>
                          </td>
                        </tr>";
                $no++;
                }
              ?>
              
          </tbody>
        </table>
      </div>        
    </div>
  </div>
    <?php 
        if ($_GET[status]==1) {
          echo "<b>Sukses, Data User Berhasil Dihapus</b>";
        }
    ?>  
</div>

 <!-- this row will not appear when printing -->
       <div class="row no-print">
          <div class="col-xs-12">
            <a href="export_karyawan.php" target="_blank" class="btn btn-success btn-sm" title="Export To Excel"><i class="fa fa-file-excel-o"></i></a>
            <a href="print_karyawan.php" target="_blank" class="btn btn-primary btn-sm" title="Print Data Karyawan"><i class="glyphicon glyphicon-print"></i></a>
          </div>
        </div>        

<?php include 'templates/footer.php';?>











