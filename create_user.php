<?php include 'templates/header.php'; include 'koneksi.php'; ?>

<div class="container">
  <div class="col-md-6">
    <div style="border: solid 1px ;padding:10px;margin-bottom:5px;background-color:white"><label><b>Input Data User</b></label></div>
    <div class="box box-primary">
      <div class="box-body">
        <form action="user/input.php" name="modal_popup" enctype="multipart/form-data" method="POST">
          <div class="form-group">
            <input type="text" name="username"  class="form-control" placeholder="USERNAME" required/>
          </div>
           <div class="form-group">
            <input type="text" name="password"  class="form-control" placeholder="PASSWORD" required/>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="submit">Save</button>
            <button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">Cancel</button>
          </div>
        </form>
      </div>
    </div>
    <?php 
      if ($_GET[status]==1) {
        echo "<b>Sukses, Data User Berhasil Tersimpan</b>";
      }
    ?>
  </div>
</div>

<?php include 'templates/footer.php';?>