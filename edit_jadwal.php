<?php include 'templates/header.php'; include 'koneksi.php'; ?>

<style type="text/css">
  th {
    text-align: center;
  }
</style>

<?php
  //PANGGIL KONEKSI DB
  include 'koneksi.php'; 

  //SQL CARI DATA JADWAL BERDASARKAN KODE JADWAL
  $sql = mysql_query("SELECT * FROM jadwal WHERE id = '$_GET[id]'");
  $dj = mysql_fetch_array($sql);
?>

  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Create Schedule</li>
  </ol>
  <div class="row">
    <div class="col-xs-8">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">EDIT SCHEDULING</h3><hr>
          <?php 
            if ($_GET[status]==1) {
              echo "<b>Data Jadwal Berhasil Diperbarui</b>";
            }
          ?>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="container">
            <div class="col-md-6">
              <div class="box box-primary">
                <form action="schedule/aksi_edit_jadwal.php" method="POST">
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-plus"></i>
                      </div>
                      <input type="hidden" name="id" value="<?php echo $dj[id]?>" required/>
                      <input type="text" class="form-control" name="po" placeholder="PURCHASE ORDER" value="<?php echo $dj[po_id]?>" required>
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-plus"></i>
                      </div>
                      <input type="text" class="form-control" name="operator" placeholder="OPERATOR" value="<?php echo $dj[operator]?>" required>
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-plus"></i>
                      </div>
                      <input type="text" class="form-control" name="brand" placeholder="BRAND" value="<?php echo $dj[brand]?>" required>
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-plus"></i>
                      </div>
                      <input type="number" class="form-control" name="qty" placeholder="QTY" value="<?php echo $dj[qty]?>" required>
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-plus"></i>
                      </div>
                      <input type="text" class="form-control" id="rangeBa" name="date" placeholder="TIME" value="<?php echo $dj[date]?>"required>
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-plus"></i>
                      </div>
                      <textarea class="form-control" name="ket" placeholder="KETERANGAN" required cols="30" rows="3" > <?php echo $dj[keterangan]?></textarea>
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="modal-footer">
                    <button class="btn btn-success" type="submit">Update</button>
                    <button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">Cancel</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<?php include 'templates/footer.php';?>