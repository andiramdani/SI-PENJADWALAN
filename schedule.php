<?php include 'templates/header.php'; include 'koneksi.php'; ?>

<style type="text/css">
  th {
    text-align: center;
  }
  .form-group {
    margin-right: 10px;
  }
  tr,th {
    font-size: 12px;
  } 
</style>

  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Schedule</li>
  </ol>
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">SCHEDULING</h3><hr>
          <div class="box box-primary">
        <div class="box-body">
          <form action="schedule.php" method="POST">
          <div class="form-inline">
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="rangeBa" name="date1" placeholder="DARI TANGGAL" required>
              </div><!-- /.input group -->
            </div><!-- /.form group -->
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="rangeBb" name="date2" placeholder="SAMPAI TANGGAL" required>
              </div><!-- /.input group -->
            </div><!-- /.form group -->
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-list"></i>
                </div>
                <select class="form-control" name="status" style="width: 135px;">
                  <option value="1">DONE</option>
                  <option value="2">UNFINISHED</option>
                </select>
              </div><!-- /.input group -->
            </div><!-- /.form group -->
            <div class="form-group">
              <div class="input-group">
              <button type="submit" name="cari" class="btn btn-default">Submit</button>
              </div>
            </div><!-- /.form group -->

            <a href="export_schedule.php?d1=<?php echo $_POST[date1]?>&d2=<?php echo $_POST[date2] ?>&status=<?php echo $_POST[status] ?>" target="_blank" class="btn btn-success" title="EXPORT TO EXCEL"><i class="fa fa-file-excel-o"></i></a>
            <a href="print_schedule.php?d1=<?php echo $_POST[date1]?>&d2=<?php echo $_POST[date2] ?>&status=<?php echo $_POST[status] ?>" target="_blank" class="btn btn-default" title="PRINT"><i class="glyphicon glyphicon-print"></i></a></a>

          </div>
        </form>
        </div>
      </div>

          <?php 
            if ($_GET[status]==1) {
              echo "<b>Jadwal Dimulai</b>";
            }
            if ($_GET[status]==2) {
              echo "<b>Jadwal Distop</b>";
            }
            if ($_GET[status_j]==1) {
              echo "<b>Jadwal Berhasil Dihapus</b>";
            }
            if ($_GET[status_j]==2) {
              echo "<b>Status Jadwal Berhasil Perbarui</b>";
            }
          ?>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Date</th>
              <th>Purchasing Order</th>
              <th>Operator</th>
              <th>Brand</th>
              <th>QTY</th>
              <th>Start</th>
              <th>Finish</th>
              <th>Actions</th>
              <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php

              // VARIABLE DATA PERIODE TANGGAL
                $date1  = $_POST[date1];
                $date2  = $_POST[date2];
                $status = $_POST[status];

              if (isset($_POST[cari])) {
               $sql  = mysql_query("SELECT * FROM jadwal WHERE start_time BETWEEN '$date1' AND '$date2' AND status = '$status'");
              }
              else
              {
                $sql = mysql_query("SELECT * FROM jadwal");
              }
              
              while ($data = mysql_fetch_array($sql)) {
                 
                 date_default_timezone_set("Asia/jakarta");
                 
                 $data[date] = date('d/m/Y', strtotime($data[date]));

                echo "
                  <tr>
                    <td>$data[date]</td>
                    <td>$data[po_id]</td>
                    <td>$data[operator]</td>
                    <td>$data[brand]</td>
                    <td align='center'>$data[qty]</td>
                    <td align='center'>$data[start_time]</td>
                    <td align='center'>$data[end_time]</td>
                    <td align='center'>" ?>
                      <a href='schedule/start_time.php?id=<?php echo $data[id]; ?>' style='margin-right:5px' data-toggle='tooltip' data-placement='top' title='START'><button class='btn btn-success' <?php if ($data[status]==1) { echo "disabled"; } ?> ><i class='glyphicon glyphicon-play'></i></button></a>
                      <a href='schedule/end_time.php?id= <?php echo $data[id]; ?>'  style='margin-right:5px' data-toggle='tooltip' data-placement='top' title='STOP'><button class='btn btn-danger' <?php if ($data[status]==1) { echo "disabled"; } ?> ><i class='glyphicon glyphicon-stop'></i></button></a>
          <?php echo "
                      <a href='edit_jadwal.php?id=$data[id]' style='margin-right:5px' data-toggle='tooltip' data-placement='top' title='EDIT'><button class='btn btn-warning'><i class='glyphicon glyphicon-pencil'></i></button></a> 
                      <a href='hapus_jadwal.php?id=$data[id]' data-toggle='tooltip' data-placement='top' title='DELETE'><button class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i></button></a>
                    <td align='center'>"; ?>
                <?php if ($data[status]==1) {
                           echo "<a href='schedule/update_status_jadwal.php?id=$data[id]&s=2' data-placement='top' title='UNCHECK'><img src='assets/icon/ok.png' width='30%'></a><br>";  
                      }
                      else {
                           echo "<a href='schedule/update_status_jadwal.php?id=$data[id]&s=1' data-placement='top' title='DONE'><img src='assets/icon/uncheck.png' width='30%'></a>";  
                      }
                ?>
        <?php echo "</td>
                  </tr>";
              }
            ?>
            </tbody>
           
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  
<?php include 'templates/footer.php';?>