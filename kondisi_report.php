<?php include 'templates/header.php'; include 'koneksi.php'; ?>

<style type="text/css">
  th {
    text-align: center;
  }
  .form-group {
    margin-right: 10px;
  }
</style>

<div class="container">
  <div class="col-md-6">
    <div style="border: solid 1px #00a65a;padding:10px;margin-bottom:5px;background-color:white;border-radius: 10px;"><label><b>Perbulan</b></label></div>
    <div class="box box-primary">
      <div class="box-body">
        <center>
          <form action="laporan/laporan_bulanan.php" name="modal_popup" enctype="multipart/form-data" method="POST">
            <div class="form-inline">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <select name ="bulan" class="form-control pull-right" required>
                    <option value="" checked>Bulan</option>
                    <option value="01">Januari</option>
                    <option value="02">Februari</option>
                    <option value="03">Maret</option>
                    <option value="04">April</option>
                    <option value="05">Mei</option>
                    <option value="06">Juni</option>
                    <option value="07">Juli</option>
                    <option value="08">Agustus</option>
                    <option value="09">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                  </select>   
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <select name="tahun" class="form-control pull-right" required>
                    <option value="" checked>Tahun</option>
                      <option value="2017">2017</option>
                      <option value="2018">2018</option>
                      <option value="2019">2019</option>
                      <option value="2020">2020</option>
                      <option value="2021">2021</option>
                      <option value="2022">2022</option>
                      <option value="2023">2023</option>
                      <option value="2024">2024</option>
                      <option value="2025">2025</option>
                      <option value="2026">2026</option>
                      <option value="2027">2027</option>
                      <option value="2028">2028</option>
                      <option value="2029">2029</option>
                      <option value="2030">2030</option>
                  </select>   
                </div><!-- /.input group -->
              </div><!-- /.form group -->
              <div class="form-group">
                <button type="submit" name="submit" class="btn btn-success">Tampilkan</button>
              </div>
            </form>      
          </center>
        </div>  
      </div>
  </div>

  <div class="col-md-5">
    <div style="border: solid 1px #00a65a;padding:10px;margin-bottom:5px;background-color:white;border-radius: 10px;"><label><b>Perstatus Kerja</b></label></div>
    <div class="box box-primary">
      <div class="box-body">
        <center>
          <form action="laporan/laporan_status.php" name="modal_popup" enctype="multipart/form-data" method="POST">
            <div class="form-inline">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <select name ="bulan" class="form-control pull-right" required>
                    <option value="" checked>Bulan</option>
                    <option value="01">Januari</option>
                    <option value="02">Februari</option>
                    <option value="03">Maret</option>
                    <option value="04">April</option>
                    <option value="05">Mei</option>
                    <option value="06">Juni</option>
                    <option value="07">Juli</option>
                    <option value="08">Agustus</option>
                    <option value="09">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                  </select>   
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <select name="tahun" class="form-control pull-right" required>
                    <option value="" checked>Tahun</option>
                      <option value="2017">2017</option>
                      <option value="2018">2018</option>
                      <option value="2019">2019</option>
                      <option value="2020">2020</option>
                      <option value="2021">2021</option>
                      <option value="2022">2022</option>
                      <option value="2023">2023</option>
                      <option value="2024">2024</option>
                      <option value="2025">2025</option>
                      <option value="2026">2026</option>
                      <option value="2027">2027</option>
                      <option value="2028">2028</option>
                      <option value="2029">2029</option>
                      <option value="2030">2030</option>
                  </select>   
                </div><!-- /.input group -->
              </div><!-- /.form group -->
              <br><br>
              <div class="form-group">
                <table>
                  <tr>
                    <td><label>Status</label></td>
                    <td width="20px"></td>
                    <td>  
                      <select class="form-control" name="status" required>
                          <option value="1">DONE</option>
                          <option value="2">UNFINISHED</option>
                      </select>
                    </td>
                    <td width="10px"></td>
                    <td><button class="btn btn-success" type="submit">Tampilkan</button></td>
                  </tr>
                </table>
              </div>
            </form>      
          </center>
        </div>  
      </div>  
  </div>

  <div class="col-md-6">
    <div style="border: solid 1px #00a65a;padding:10px;margin-bottom:5px;background-color:white;border-radius: 10px;"><label><b>Purchases Order</b></label></div>
    <div class="box box-primary">
      <div class="box-body">
        <center>
          <form action="laporan/laporan_po.php" name="modal_popup" enctype="multipart/form-data" method="POST">
            <div class="form-inline">
              <div class="form-group">
                <table>
                  <tr>
                    <td><label>Pilih PO</label></td>
                    <td width="20px"></td>
                    <td>  
                      <select class="form-control" name="po" required>
                        <option value="-">-</option>
                        <?php
                          //TAMPILKAN LIST NAMA PEMBIMBING
                          include 'koneksi.php';
                          $sql = mysql_query("SELECT po_id from jadwal group by po_id");
                          while ($nama = mysql_fetch_array($sql)) {
                                echo "<option value='$nama[po_id]'>$nama[po_id]</option>";
                          }
                        ?>
                         
                      </select>
                    </td>
                    <td width="10px"></td>
                    <td><button class="btn btn-success" type="submit">Tampilkan</button></td>
                  </tr>
                </table>
              </div>
            </form>      
          </center>
        </div>  
      </div>  
  </div>
  <div class="col-md-5">
    <div style="border: solid 1px #00a65a;padding:10px;margin-bottom:5px;background-color:white;border-radius: 10px;"><label><b>Brand</b></label></div>
    <div class="box box-primary">
      <div class="box-body">
        <center>
          <form action="laporan/laporan_brand.php" name="modal_popup" enctype="multipart/form-data" method="POST">
            <div class="form-inline">
              <div class="form-group">
                <table>
                  <tr>
                    <td><label>Pilih Brand</label></td>
                    <td width="20px"></td>
                    <td>  
                      <select class="form-control" name="brand" required>
                        <option value="-">-</option>
                        <?php
                          //TAMPILKAN LIST NAMA PEMBIMBING
                          include 'koneksi.php';
                          $sql = mysql_query("SELECT brand from jadwal group by brand");
                          while ($nama = mysql_fetch_array($sql)) {
                                echo "<option value='$nama[brand]'>$nama[brand]</option>";
                          }
                        ?>
                         
                      </select>
                    </td>
                    <td width="10px"></td>
                    <td><button class="btn btn-success" type="submit">Tampilkan</button></td>
                  </tr>
                </table>
              </div>
            </form>      
          </center>
        </div>  
      </div>  
  </div>

  <div class="col-md-6">
    <div style="border: solid 1px #00a65a;padding:10px;margin-bottom:5px;background-color:white;border-radius: 10px;"><label><b>Operator</b></label></div>
    <div class="box box-primary">
      <div class="box-body">
        <center>
          <form action="laporan/laporan_operator.php" name="modal_popup" enctype="multipart/form-data" method="POST">
            <div class="form-inline">
              <div class="form-group">
                <table>
                  <tr>
                    <td><label>Pilih Operator</label></td>
                    <td width="20px"></td>
                    <td>  
                      <select class="form-control" name="operator" required>
                        <option value="-">-</option>
                        <?php
                          //TAMPILKAN LIST NAMA PEMBIMBING
                          include 'koneksi.php';
                          $sql = mysql_query("SELECT operator from jadwal group by operator");
                          while ($nama = mysql_fetch_array($sql)) {
                                echo "<option value='$nama[operator]'>$nama[operator]</option>";
                          }
                        ?>
                         
                      </select>
                    </td>
                    <td width="10px"></td>
                    <td><button class="btn btn-success" type="submit">Tampilkan</button></td>
                  </tr>
                </table>
              </div>
            </form>      
          </center>
        </div>  
      </div>  
  </div>

  <div class="col-md-5">
    <div style="border: solid 1px #00a65a;padding:10px;margin-bottom:5px;background-color:white;border-radius: 10px;"><label><b>Perperiode</b></label></div>
    <div class="box box-primary">
      <div class="box-body">
        <center>
          <form action="laporan/laporan_perperiode.php" name="modal_popup" enctype="multipart/form-data" method="POST">
            <div class="form-inline">
                <div class="form-group">
                  <table>
                    <tr>
                      <td><input type="text" class="form-control" id="rangeBa" name="date1" placeholder="DARI TANGGAL" required></td>
                      <td width="10px"></td>
                      <td><input type="text" class="form-control" id="rangeBb" name="date2" placeholder="SAMPAI TANGGAL" required></td>
                      <td width="10px"></td>
                      <td><button class="btn btn-success" type="submit">Tampilkan</button></td>
                    </tr>
                  </table>
                </div>
            </div>
          </form>
        </center>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div style="border: solid 1px #00a65a;padding:10px;margin-bottom:5px;background-color:white;border-radius: 10px;"><label><b>Pertahun</b></label></div>
    <div class="box box-primary">
      <div class="box-body">
        <center>
          <form action="laporan/laporan_pertahun.php" name="modal_popup" enctype="multipart/form-data" method="POST">
            <div class="form-inline">
                <div class="form-group">
                  <table>
                    <tr>
                      <td><label>TAHUN</label></td>
                      <td width="20px"></td>
                      <td>  
                        <select class="form-control" name="tahun" required>
                          <option value="2017">2017</option>
                          <option value="2018">2018</option>
                          <option value="2019">2019</option>
                          <option value="2020">2020</option>
                          <option value="2021">2021</option>
                          <option value="2022">2022</option>
                          <option value="2023">2023</option>
                          <option value="2024">2024</option>
                          <option value="2025">2025</option>
                          <option value="2026">2026</option>
                          <option value="2027">2027</option>
                          <option value="2028">2028</option>
                          <option value="2029">2029</option>
                          <option value="2030">2030</option>
                        </select>
                      </td>
                      <td width="10px"></td>
                      <td><button class="btn btn-success" type="submit">Tampilkan</button></td>
                    </tr>
                  </table>
                </div>
              </div>
            </form>      
          </center>
        </div>  
      </div>
  </div>

  <div style="margin-top:250px"></div>

  

</div>


    

  
<?php include 'templates/footer.php';?>