<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SI-PENJADWALAN</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="assets/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="assets/bower_components/jvectormap/jquery-jvectormap.css">
  
  <link rel="stylesheet" href="assets/plugins/daterangepicker/jquery-ui.css" />
  <link rel="stylesheet" type="text/css" href="assets/css/jquery.ui.1.10.3.ie.css">
  <link rel="stylesheet" type="text/css" href="assets/css/jquery-ui-1.10.3.custom.css">
  <link rel="stylesheet" type="text/css" href="assets/css/jquery-ui-1.10.3.theme.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="assets/css/jquery.ui.1.10.3.ie.css">
  <link rel="stylesheet" type="text/css" href="assets/css/jquery-ui-1.10.3.custom.css">
  <link rel="stylesheet" type="text/css" href="assets/css/jquery-ui-1.10.3.theme.css">

  <style type="text/css">
    th{
      text-align: center;
    }
    .form-group {
      margin-right: 10px;
    }
  </style>
  
</head>
<body class="hold-transition">



  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <center><img src="image/logo.png" width="5%">
          <h2>SISTEM INFORMASI PENJADWALAN KERJA - PT. LIMASATRIA</h2></center><hr>
          
          <div style="float:right">
            <a href="index.php"><button class="btn btn-warning">PENJADWALAN</button></a>
            <a href="form_login.php"><button class="btn btn-danger">Login</button></a>
          </div><br><br><br>

        <div class="box box-primary">
        <div class="box-body" style='float:right'>
          <form action="index.php" method="POST">
          <div class="form-inline">
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="rangeBa" name="date1" placeholder="DARI TANGGAL" required>
              </div><!-- /.input group -->
            </div><!-- /.form group -->
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="rangeBb" name="date2" placeholder="SAMPAI TANGGAL" required>
              </div><!-- /.input group -->
            </div><!-- /.form group -->
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="glyphicon glyphicon-user"></i>
                </div>
                <select class="form-control" name="operator" style="width: 150px;" required>
                  <option value="-">NAMA OPERATOR</option>
                  <?php
                    //TAMPILKAN LIST NAMA PEMBIMBING
                    include 'koneksi.php';
                    $sql = mysql_query("SELECT operator from jadwal group by id");
                    while ($nama = mysql_fetch_array($sql)) {
                          echo "<option value='$nama[operator]'>$nama[operator]</option>";
                    }
                  ?>
                </select>
              </div><!-- /.input group -->
            </div><!-- /.form group -->
            <div class="form-group">
              <div class="input-group">
              <button type="submit" name="cari" class="btn btn-success">Tampilkan</button>
              </div>
            </div><!-- /.form group -->

          </div>
        </form>
        </div>
      </div>

          <?php 
            if ($_GET[status]==1) {
              echo "<b>Jadwal Dimulai</b>";
            }
            if ($_GET[status]==2) {
              echo "<b>Jadwal Distop</b>";
            }
            if ($_GET[status_j]==1) {
              echo "<b>Jadwal Berhasil Dihapus</b>";
            }
            if ($_GET[status_j]==2) {
              echo "<b>Status Jadwal Berhasil Perbarui</b>";
            }
          ?>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped table-hover">
            <thead>
            <tr style='background-color: #337ab7;color: white;'>
              <th>Date</th>
              <th>Purchasing Order</th>
              <th>Operator</th>
              <th>Brand</th>
              <th>QTY</th>
              <th>Start</th>
              <th>Finish</th>
              <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php

              include 'koneksi.php';

              // VARIABLE DATA PERIODE TANGGAL
                $date1  = $_POST[date1];
                $date2  = $_POST[date2];
                $operator = $_POST[operator];

              if (isset($_POST[cari])) {
               $sql  = mysql_query("SELECT * FROM jadwal WHERE date BETWEEN '$date1' AND '$date2' AND operator = '$operator'");
              }
              else
              {
                $sql = mysql_query("SELECT * FROM jadwal");
              }
              
              while ($data = mysql_fetch_array($sql)) {
                 
                 date_default_timezone_set("Asia/jakarta");
                 
                 $data[date] = date('d/m/Y', strtotime($data[date]));

                if ($data[status]==1) {
                     $data[status] = 'Done';
                }
                if ($data[status]==2) {
                    $data[status] = 'Unfinished';
                }

                echo "
                  <tr>
                    <td align='center'>$data[date]</td>
                    <td align='center'>$data[po_id]</td>
                    <td>$data[operator]</td>
                    <td>$data[brand]</td>
                    <td align='center'>$data[qty]</td>
                    <td align='center'>$data[start_time]</td>
                    <td align='center'>$data[end_time]</td>
                    <td align='center'>$data[status]</td>
                  </tr>";
              }
            ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
      </div>
      <strong>Copyright &copy; 2017 - RIAN SUPRIANTO - SI PENJADWALAN KERJA</strong>
    </footer>
  </div>
  <!-- /.row -->

  <!-- jQuery 3 -->
<script src="assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="assets/bower_components/raphael/raphael.min.js"></script>

<!-- Sparkline -->
<script src="assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- date-range-picker -->
<script src="assets/plugins/daterangepicker/moment.min.js"></script>
<script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="assets/dist/js/adminlte.min.js"></script>


<script>
  $(function () {
    $('#example1').DataTable()
  })
</script>

<script type="text/javascript">       
        // Select a Date Range with datepicker
        $( "#rangeBa" ).datepicker({
            defaultDate: "",
            dateFormat:'yy-mm-dd',
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function( selectedDate ) {
                $( "#rangeBb" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#rangeBb" ).datepicker({
            defaultDate: "+1w",
            dateFormat:'yy-mm-dd',
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function( selectedDate ) {
                $( "#rangeBa" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    </script>
 
</body>
</html>
