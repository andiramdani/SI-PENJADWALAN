<?php include 'templates/header.php'; include 'koneksi.php'; ?>

<style type="text/css">
  th {
    text-align: center;
  }
  .form-group {
    margin-right: 10px;
  }
</style>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Schedule</li>
  </ol>
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">WORK</h3><hr>
          <?php 
            if ($_GET[status]==1) {
              echo "<b>Jadwal Dimulai</b>";
            }
            if ($_GET[status]==2) {
              echo "<b>Jadwal Distop</b>";
            }
            if ($_GET[status_j]==1) {
              echo "<b>Jadwal Berhasil Dihapus</b>";
            }
            if ($_GET[status_j]==2) {
              echo "<b>Status Jadwal Berhasil Perbarui</b>";
            }
          ?>

          <div class="box-body">
          <form action="work.php" method="POST">
          <div class="form-inline">
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="rangeBa" name="date1" placeholder="DARI TANGGAL" required>
              </div><!-- /.input group -->
            </div><!-- /.form group -->
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="rangeBb" name="date2" placeholder="SAMPAI TANGGAL" required>
              </div><!-- /.input group -->
            </div><!-- /.form group -->
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-list"></i>
                </div>
                <select class="form-control" name="status" style="width: 130px;">
                  <option value="1">DONE</option>
                  <option value="2">UNFINISHED</option>
                </select>
              </div><!-- /.input group -->
            </div><!-- /.form group -->
            <div class="form-group">
              <div class="input-group">
              <button type="submit" name="cari" class="btn btn-primary">Submit</button>
              </div>
            </div><!-- /.form group -->

            <a href="export_work.php?d1=<?php echo $_POST[date1]?>&d2=<?php echo $_POST[date2] ?>&status=<?php echo $_POST[status] ?>" target="_blank" class="btn btn-success" title="EXPORT TO EXCEL"><i class="fa fa-file-excel-o"></i></a>
            <a href="print_work.php?d1=<?php echo $_POST[date1]?>&d2=<?php echo $_POST[date2] ?>&status=<?php echo $_POST[status] ?>" target="_blank" class="btn btn-default" title="PRINT"><i class="glyphicon glyphicon-print"></i></a></a>

          </div>
        </form>
        </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Purchasing Order</th>
              <th>Operator</th>
              <th>Brand</th>
              <th>QTY</th>
              <th>Time</th>
              <th>Description</th>
              <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php
               // VARIABLE DATA PERIODE TANGGAL
                $date1  = $_POST[date1];
                $date2  = $_POST[date2];
                $status = $_POST[status];

                if (isset($_POST[cari])) {
                  $sql = mysql_query("SELECT po_id,operator,brand,qty,keterangan,status,SEC_TO_TIME(SUM((TIME_TO_SEC(TIMEDIFF(end_time,start_time))))) as jam_kerja FROM jadwal WHERE start_time BETWEEN '$date1' AND '$date2' AND status = '$status' GROUP BY id");
                }
                else
                {
                  $sql = mysql_query("SELECT po_id,operator,brand,qty,keterangan,status,SEC_TO_TIME(SUM((TIME_TO_SEC(TIMEDIFF(end_time,start_time))))) as jam_kerja FROM jadwal GROUP BY id");
                }

              while ($data = mysql_fetch_array($sql)) {
                echo "
                  <tr>
                    <td>$data[po_id]</td>
                    <td>$data[operator]</td>
                    <td>$data[brand]</td>
                    <td align='center'>$data[qty]</td>
                    <td align='center'>$data[jam_kerja]</td>
                    <td>$data[keterangan]</td>
                    <td align='center'>"; ?>
                <?php if ($data[status]==1) {
                           echo "DONE";  
                      }
                      else {
                           echo "-";  
                      }
                ?>
        <?php echo "</td>
                  </tr>";
              }
            ?>
            </tbody>
            <tfoot>
            <tr>
              <th>Purchasing Order</th>
              <th>Operator</th>
              <th>Brand</th>
              <th>QTY</th>
              <th>Time</th>
              <th>Description</th>
              <th>Status</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<?php include 'templates/footer.php';?>