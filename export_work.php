<?php

// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=laporan_hasil_kerja-export.xls");

?>

<h3>LAPORAN HASIL KERJA</H3>

<?php 
      date_default_timezone_set("Asia/jakarta");

  if (!$_GET[d1]=='' AND !$_GET[d2]=='' AND !$_GET[status]=='') {
      
      $d1 = date('d/m/Y', strtotime($_GET[d1]));
      $d2 = date('d/m/Y', strtotime($_GET[d2]));

      if ($_GET[status]==1) {
            $status = 'Done';
      }
      if ($_GET[status]==2) {
            $status = 'Unfinished';
      }

      echo "
          <table>
            <tr>
              <td>Periode</td><td>: $d1  - $d2</td>
            </tr>
            <tr>
              <td>Status</td><td>: $status</td>
            </tr>
          </table>";

  }

?>

 <table border="1">
    <thead>
    <tr>
      <th>Purchasing Order</th>
      <th>Operator</th>
      <th>Brand</th>
      <th>QTY</th>
      <th>Time</th>
      <th>Description</th>
      <th>Status</th>
    </tr>
    </thead>
    <tbody>
    <?php

       include 'koneksi.php'; 

       // VARIABLE DATA PERIODE TANGGAL
        $date1  = $_GET[d1];
        $date2  = $_GET[d2];
        $status = $_GET[status];

        if (isset($_POST[cari])) {
          $sql = mysql_query("SELECT po_id,operator,brand,qty,keterangan,status,SEC_TO_TIME(SUM((TIME_TO_SEC(TIMEDIFF(end_time,start_time))))) as jam_kerja FROM jadwal WHERE start_time BETWEEN '$date1' AND '$date2' AND status = '$status' GROUP BY id");
        }
        else
        {
          $sql = mysql_query("SELECT po_id,operator,brand,qty,keterangan,status,SEC_TO_TIME(SUM((TIME_TO_SEC(TIMEDIFF(end_time,start_time))))) as jam_kerja FROM jadwal WHERE start_time BETWEEN '$date1' AND '$date2' AND status = '$status' GROUP BY id");
        }

      while ($data = mysql_fetch_array($sql)) {
        
        echo "
          <tr>
            <td>$data[po_id]</td>
            <td>$data[operator]</td>
            <td>$data[brand]</td>
            <td align='center'>$data[qty]</td>
            <td align='center'>$data[jam_kerja]</td>
            <td>$data[keterangan]</td>
            <td align='center'>"; ?>
        <?php if ($data[status]==1) {
                   echo "DONE";  
              }
              else {
                   echo "-";  
              }
        ?>
<?php echo "</td>
          </tr>";
      }
    ?>
    </tbody>
   
  </table>