<?php include 'templates/header.php'; include 'koneksi.php'; ?>

<style type="text/css">
  th {
    text-align: center;
  }
</style>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Create Schedule</li>
  </ol>
  <div class="row">
    <div class="col-xs-8">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">CREATE SCHEDULING</h3><hr>
          <?php 
            if ($_GET[status]==1) {
              echo "<b>Data Jadwal Berhasil Tersimpan</b>";
            }
          ?>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="container">
            <div class="col-md-6">
              <div class="box box-primary">
                <form action="schedule/aksi_input_jadwal.php" method="POST">
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-plus"></i>
                      </div>
                      <input type="text" class="form-control" name="po" placeholder="PURCHASE ORDER" required>
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-plus"></i>
                      </div>
                       <select class="form-control" name="operator" required>
                        <option value="-">Nama Karyawan</option>
                        <?php
                          //TAMPILKAN LIST NAMA PEMBIMBING
                          include 'koneksi.php';
                          $sql = mysql_query("SELECT nama from karyawan group by id");
                          while ($nama = mysql_fetch_array($sql)) {
                                echo "<option value='$nama[nama]'>$nama[nama]</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-plus"></i>
                      </div>
                      <input type="text" class="form-control" name="brand" placeholder="BRAND" required>
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-plus"></i>
                      </div>
                      <input type="number" class="form-control" name="qty" placeholder="QTY" required>
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-plus"></i>
                      </div>
                      <input type="text" class="form-control" id="rangeBa" name="date" placeholder="DATE" required>
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-plus"></i>
                      </div>
                      <textarea class="form-control" name="ket" placeholder="KETERANGAN" required cols="30" rows="3"></textarea>
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="modal-footer">
                    <button class="btn btn-success" type="submit">Save</button>
                    <button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">Cancel</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<?php include 'templates/footer.php';?>