<?php include 'templates/header.php'; include 'koneksi.php'; ?>

<?php
  //PANGGIL KONEKSI DB
  include 'koneksi.php'; 

  //SQL CARI DATA KARYAWAN BERDASARKAN KODE ID
  $sql = mysql_query("SELECT * FROM karyawan WHERE id = '$_GET[id]'");
  $dp = mysql_fetch_array($sql);
?>


<div class="container">
  <div class="col-md-6">
    <div style="border: solid 1px blue;padding:10px;margin-bottom:5px;background-color:white"><label><b>Edit Data Pembimbing</b></label></div>
    <div class="box box-primary">
      <div class="box-body">
        <form action="karyawan/update.php" name="modal_popup" enctype="multipart/form-data" method="POST">
           <div class="form-group">
            <input type="text" name="nik"  class="form-control" placeholder="NIK" value="<?php echo $dp[nik]?>" required/>
          </div>
          <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $dp[id]?>" required/>
            <input type="text" name="nama"  class="form-control" placeholder="NAMA KARYAWAN" value="<?php echo $dp[nama]?>" required/>
          </div>
          <div class="form-group">
            <label>Jenis Kelamin</label>
            <div class="input-group">
              <select class="form-control" name="jk">
                <option value="Pria" <?php if ($dp[jenis_kelamin]==Pria){ echo "selected";}?>>Laki-Laki</option>
                <option value="Wanita" <?php if ($dp[jenis_kelamin]==Wanita){ echo "selected";}?>>Perempuan</option>
              </select>
            </div><!-- /.input group -->
          </div><!-- /.form group -->
          <div class="form-group">
            <input type="text" name="jabatan"  class="form-control" placeholder="JABATAN / POSISI STAFF" value="<?php echo $dp[jabatan]?>" required/>
          </div>
          <div class="form-group">
            <input type="text" name="hp"  class="form-control" placeholder="TELEPON" style="width:180px" value="<?php echo $dp[hp]?>" required/>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="submit">Update</button>
            <button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">Cancel</button>
          </div>
        </form>
      </div>
    </div>
    <?php 
      if ($_GET[status]==1) {
        echo "<b>Sukses, Data karyawan Berhasil Diperbarui</b>";
      }
    ?>
  </div>
</div>

<?php include 'templates/footer.php';?>