<?php

// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=laporan_penjadwalan_kerja-export.xls");

?>

<h3>LAPORAN PENJADWALAN KERJA</H3>

<?php 

  date_default_timezone_set("Asia/jakarta");

  if (!$_GET[d1]=='' AND !$_GET[d2]=='' AND !$_GET[status]=='') {
      
      $d1 = date('d/m/Y', strtotime($_GET[d1]));
      $d2 = date('d/m/Y', strtotime($_GET[d2]));

      if ($_GET[status]==1) {
            $status = 'Done';
      }
      if ($_GET[status]==2) {
            $status = 'Unfinished';
      }

      echo "
          <table>
            <tr>
              <td>Periode</td><td>: $d1  - $d2</td>
            </tr>
            <tr>
              <td>Status</td><td>: $status</td>
            </tr>
          </table>";

  }

?>

<table border="1">
  <thead>
    <tr>
      <th>Date</th>
      <th>Purchasing Order</th>
      <th>Operator</th>
      <th>Brand</th>
      <th>QTY</th>
      <th>Start</th>
      <th>Finish</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    <?php

      date_default_timezone_set("Asia/jakarta");

       include 'koneksi.php'; 

      // VARIABLE DATA PERIODE TANGGAL
        $date1  = $_GET[d1];
        $date2  = $_GET[d2];
        $status = $_GET[status];

      if (isset($_GET[d1]) AND isset($_GET[d2]) AND isset($_GET[status])) {
        $sql  = mysql_query("SELECT * FROM jadwal WHERE start_time BETWEEN '$date1' AND '$date2' AND status = '$status'");
      }
      if($_GET[d1]=='' AND $_GET[d2]=='' AND $_GET[status]=='' ) {
        $sql = mysql_query("SELECT * FROM jadwal");
      }
      
      while ($data = mysql_fetch_array($sql)) {

        $data[date] = date('d/m/Y', strtotime($data[date]));

        if ($data[status]==1) {
            $data[status] = 'Done';
        }
        if ($data[status]==2) {
            $data[status] = 'Unfinished';
        }

        echo "
          <tr>
            <td>$data[date]</td>
            <td>$data[po_id]</td>
            <td>$data[operator]</td>
            <td>$data[brand]</td>
            <td align='center'>$data[qty]</td>
            <td align='center'>$data[start_time]</td>
            <td align='center'>$data[end_time]</td>
            <td align='center'>$data[status]</td>
          </tr>";
      }
    ?>
  </tbody>
</table>